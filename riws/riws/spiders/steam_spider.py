import scrapy, re
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from riws.items import GameItem, GameItemLoader

class SteamSpider(CrawlSpider):
    name = 'steam'
    allowed_domains = ['steampowered.com']
    start_urls = [
        'http://store.steampowered.com/'
    ]

    rules = (
        Rule(LinkExtractor(
            allow=('app\/\d+\/'),
            deny=('app\/\d+\/\?l=.+')),
            callback='parse_app'),
    )

    def parse_app(self, response):
        if self._is_agecheck_link(response.url):
            scrapy.log.msg('Agecheck required for url: ' + response.url)
            return self._do_agecheck(response)
        else:
            loader = GameItemLoader(item=GameItem(), response=response)
            loader.add_value('url', response.url)
            self._parse_title(loader)
            self._parse_price(loader)
            self._parse_original_price(loader)
            self._parse_discount(loader)
            self._parse_category(loader)
            self._parse_description(loader)
            self._parse_score(loader)
            return loader.load_item()

    def _do_agecheck(self, response):
        scrapy.log.msg('Doing agecheck...')
        return scrapy.FormRequest(
            response.url,
            formdata = {
                'snr': '1_agecheck_agecheck__age-gate',
                'ageDay': '1',
                'ageMonth': 'January',
                'ageYear': '1900'
            },
            callback = self.parse_app
        )

    def _is_agecheck_link(self, url):
        return re.match('.+agecheck\/app\/\d+\/.*', url)

    def _parse_title(self, loader):
        loader.add_xpath('title', '//div[contains(@class, "apphub_AppName")]/text()')

    def _parse_price(self, loader):
        loader.add_xpath('price', '//div[contains(@class, "discount_final_price") or contains(@class, "game_purchase_price")]/text()')

    def _parse_original_price(self, loader):
        loader.add_xpath('original_price', '//div[contains(@class, "discount_original_price")]/text()')

    def _parse_discount(self, loader):
        loader.add_xpath('discount', '//div[contains(@class, "discount_pct")]/text()')

    def _parse_category(self, loader):
        loader.add_xpath('category', '//div[contains(@class, "details_block")][1]/a[contains(@href, "genre")]/text()')

    def _parse_description(self, loader):
        loader.add_xpath('description', '//div[@id="game_area_description"]/text()')

    def _parse_score(self, loader):
        loader.add_xpath('score', '//div[@id="game_area_metascore"]/span[not(@*)]/text()')
