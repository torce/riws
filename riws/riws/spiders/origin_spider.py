import scrapy, re
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from riws.items import GameItem, GameItemLoader

class OriginSpider(CrawlSpider):
    name = 'origin'
    allowed_domains = ['origin.com']
    start_urls = [
        'https://www.origin.com/'
    ]

    rules = (
        Rule(LinkExtractor(
            allow=('store\/buy\/.+')),
            callback='parse_app'),
    )

    def parse_app(self, response):
        loader = GameItemLoader(item=GameItem(), response=response)
        loader.add_value('url', response.url)
        self._parse_title(loader)
        self._parse_price(loader)
        self._parse_original_price(loader)
        self._parse_discount(loader)
        self._parse_category(loader)
        self._parse_description(loader)
        self._parse_score(loader)
        return loader.load_item()

    def _parse_title(self, loader):
        loader.add_xpath('title', '//div[@id="title"]//h1/text()')

    def _parse_price(self, loader):
        loader.add_xpath('price', '//p[contains(@class, "actual-price")]/text()')

    def _parse_original_price(self, loader):
        loader.add_xpath('original_price', '//p[contains(@class, "sale-original-price")]/text()')

    def _parse_discount(self, loader):
        loader.add_xpath('discount', '//p[contains(@class, "sale-original-save")]/text()')

    def _parse_category(self, loader):
        loader.add_xpath('category', '//table[contains(@class, "game-info")]/tr[1]//a/text()')

    def _parse_description(self, loader):
        loader.add_xpath('description', '//div[@id="details"]/div/div[1]/div[1]//text()')

    def _parse_score(self, loader):
        pass #loader.add_xpath('score', '//div[@id="game_area_metascore"]/span[not(@*)]/text()')
