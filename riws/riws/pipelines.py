# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import re
from scrapy.exceptions import DropItem

class RiwsPipeline(object):
    def process_item(self, item, spider):
        return item

class DuplicatesPipeline(object):
    def __init__(self):
        self.steam_ids = set()
        self.origin_ids = set()

    def process_item(self, item, spider):
        if spider.name == 'steam':
            return self._process_steam_item(item)
        else: #Origin
            return self._process_origin_item(item)

    def _process_steam_item(self, item):
        if self._extract_steam_id(item) in self.steam_ids:
            raise DropItem("Duplicate item found: %s" % item)
        else:
            self.steam_ids.add(self._extract_steam_id(item))
            return item

    def _extract_steam_id(self, item):
        return re.search('app\/\d+', item['url']).group(0).split('/')[1]

    def _process_origin_item(self, item):
        if self._extract_origin_id(item) in self.origin_ids:
            raise DropItem("Duplicate item found: %s" % item)
        else:
            self.origin_ids.add(self._extract_origin_id(item))
            return item

    def _extract_origin_id(self, item):
        return re.search('store\/buy\/[a-zA-Z0-9-]+', item['url']).group(0).split('/')[2]

class DefaultItemValues(object):

    def process_item(self, item, spider):
        if item.get('discount') == None:
            item['discount'] = 0
        if item.get('price') == None:
            item['price'] = 0
        if item.get('original_price') == None:
            item['original_price'] = item.get('price') + (item.get('price') * item.get('discount') / 100.0)
        return item
