# -*- coding: utf-8 -*-

# Scrapy settings for riws project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'riws'

SPIDER_MODULES = ['riws.spiders']
NEWSPIDER_MODULE = 'riws.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'riws (+http://www.yourdomain.com)'

DEFAULT_REQUEST_HEADERS = {
    'Accept-Language': 'es',
}

ITEM_PIPELINES = {
    'riws.pipelines.DuplicatesPipeline': 300,
    'riws.pipelines.DefaultItemValues': 301,
    'scrapysolr.SolrPipeline': 302
}

SOLR_URL = 'http://localhost:8983/solr/'
SOLR_MAPPING = {
    'id': 'url',
    'title': 'title',
    'price': 'price',
    'original_price': 'original_price',
    'discount': 'discount',
    'category': 'category',
    'description': 'description',
    'metascore': 'score'
}
