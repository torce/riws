# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import log
import re
from scrapy.contrib.loader import ItemLoader
from scrapy.contrib.loader.processor import TakeFirst, Join, Compose

class GameItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    original_price = scrapy.Field()
    discount = scrapy.Field()
    category = scrapy.Field()
    description = scrapy.Field()
    score = scrapy.Field()
    url = scrapy.Field()

class GameItemLoader(ItemLoader):
    title_out = Compose(
        TakeFirst(),
        lambda b: re.sub('[\t\n]', '', b).strip()
    )
    price_out = Compose(
        TakeFirst(),
        lambda a: re.sub(',', '.', a),
        lambda a: re.sub('[^\d.]+', '', a),
        lambda a: float(a) if len(a) > 0 else None
    )
    original_price_out = Compose(
        TakeFirst(),
        lambda a: re.sub(',', '.', a),
        lambda a: re.sub('[^\d.]+', '', a),
        lambda a: float(a) if len(a) > 0 else None
    )
    discount_out = Compose(
        TakeFirst(),
        lambda a: re.sub('[^\d.]+', '', a),
        int
    )
    description_out = Compose(
        lambda a: map(lambda b: re.sub('[\t\n]', '', b).strip(), a),
        lambda a: filter(None, a),
        Join()
    )
    score_out = Compose(
        TakeFirst(),
        int
    )
    url_out = TakeFirst()
